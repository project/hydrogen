<?php
/**
 * Advantage Labs base page.tpl.php based on ninesixty
 */
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>

<body class="<?php print $body_classes; ?>">
  <div id="page-top"></div>
  <div id="page-wrapper">
    <div id="page" class="<?php print $grid_page; ?> clear-block">

      <?php if ($header): ?>
        <div id="header-region" class="region <?php print $grid_header_region; ?> clear-block">
          <?php print $header; ?>
        </div>
      <?php endif; ?>

      <div id="site-header" class="clear-block">
        <?php if (!$linked_logo_img && !$linked_site_name && !$site_slogan) print '<a href="/">'; ?>
          <div id="branding" class="<?php print $grid_branding; ?> clear-block">
            <?php if ($linked_logo_img): ?>
              <span id="logo" class="<?php print $grid_logo; ?>"><?php print $linked_logo_img; ?></span>
            <?php endif; ?>

            <?php if ($linked_site_name): ?>
              <h1 id="site-name" class="<?php print $grid_site_name; ?>"><?php print $linked_site_name; ?></h1>
            <?php endif; ?>

            <?php if ($site_slogan): ?>
              <div id="site-slogan" class="<?php print $grid_site_slogan; ?>"><?php print $site_slogan; ?></div>
            <?php endif; ?>
          </div>
        <?php if (!$linked_logo_img && !$linked_site_name && !$site_slogan) print '</a>'; ?>

        <?php if ($main_menu_links || $secondary_menu_links || $site_navigation_region): ?>
          <div id="site-menu" class="<?php print $grid_site_menu; ?>">
            <?php print $site_navigation_region; ?>
            <?php print $main_menu_links; ?>
            <?php print $secondary_menu_links; ?>
          </div>
        <?php endif; ?>

        <?php if ($search_box): ?>
          <div id="search-box" class="<?php print $grid_search-box; ?>"><?php print $search_box; ?></div>
        <?php endif; ?>
      </div> <!-- // end #site-header -->

      <?php if ($mission || $header): ?>
        <div id="site-subheader" class="prefix-1 suffix-1 clear-block">
          <?php if ($mission): ?>
            <div id="mission" class="<?php print $grid_mission; ?>">
              <?php print $mission; ?>
            </div>
          <?php endif; ?>
        </div> 
      <?php endif; ?>

      <?php if ($feature): ?>
        <div id="feature" class="column <?php print $grid_feature; ?> alpha omega">
          <?php print $feature; ?>
        </div>
      <?php endif; ?>

      <?php if ($sidebar_first  && ($grid_layout == 'sidebar_first')): ?>
        <div id="sidebar-first" class="column sidebar region <?php print $grid_sidebar_first ?>">
          <?php print $sidebar_first; ?>
        </div>
      <?php endif; ?>

      <div id="content-wrapper" class="<?php print $grid_content_wrapper; ?>">
        <?php if ($subfeature): ?>
          <div id="subfeature" class="region clear-block <?php print $grid_sub_feature; ?>">
            <?php print $subfeature; ?>
          </div>
        <?php endif; ?>

        <?php if ($sidebar_first  && (in_array($grid_layout, array('sidebar_second', 'equal')))): ?>
          <div id="sidebar-first" class="column sidebar region <?php print $grid_sidebar_first ?>">
            <?php print $sidebar_first; ?>
          </div>
        <?php endif; ?>

        <div id="main" class="column <?php print $grid_main; ?>">
          <?php print $pre_content; ?>
          <?php print $breadcrumb; ?>
          <div id="main-content" class="region clear-block">
            <?php if ($title): ?>
              <h1 class="title" id="page-title"><?php print $title; ?></h1>
            <?php endif; ?>
            <?php if ($subtitle): ?>
              <span class="subtitle" id="page-subtitle"><?php print $subtitle; ?></span>
            <?php endif; ?>

            <?php if ($tabs): ?>
              <div class="tabs"><?php print $tabs; ?></div>
            <?php endif; ?>

            <?php print $messages; ?>
            <?php print $help; ?>

            <?php if ($content_inset): ?>
              <div id="content-inset" class="column region <?php print $grid_inset; ?>">
                <?php print $content_inset; ?>
              </div>
            <?php endif; ?>

            <?php print $content; ?>
            <?php print $feed_icons; ?>
            <?php print $post_content; ?>
          </div> <!-- // end #main-content -->
        </div> <!-- // end #main -->

        <?php if ($sidebar_first  && (in_array($grid_layout, array('both_sidebar_second')))): ?>
          <div id="sidebar-first" class="column sidebar region <?php print $grid_sidebar_first ?>">
            <?php print $sidebar_first; ?>
          </div>
        <?php endif; ?>

        <?php if ($sidebar_second && (in_array($grid_layout, array('sidebar_first', 'equal')))): ?>
          <div id="sidebar-second" class="column sidebar region <?php print $grid_sidebar_second; ?>">
            <?php print $sidebar_second; ?>
          </div>
        <?php endif; ?>

      </div> <!-- // end #content-wrapper -->

      <?php if ($sidebar_second && (in_array($grid_layout, array('sidebar_second', 'both_sidebar_second')))): ?>
        <div id="sidebar-second" class="column sidebar region <?php print $grid_sidebar_second; ?>">
          <?php print $sidebar_second; ?>
        </div>
      <?php endif; ?>

    </div> <!-- // end #page -->
  </div> <!-- // end #page-wrapper -->

  <div id="page-end"></div>

  <?php if ($footer || $footer_message): ?>
    <div id="footer" class="<?php print $grid_footer; ?>">
      <div id="footer-region" class="region clear-block">
        <?php print $footer; ?>
        <?php if ($footer_message): ?>
          <div id="footer-message" class="<?php print $grid_footer_message; ?>">
            <?php print $footer_message; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  <?php endif; ?>

  <?php print $closure; ?>
</body>
</html>
