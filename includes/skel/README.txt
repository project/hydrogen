
SUB THEME SKELETON
------------------
This directory contains a skeleton of a sub theme for Hydrogen 

TO USE:
-------

1) Copy contents of hydrogen/includes/skel to a folder in your themes folder, IE: mysubtheme.
2) Rename theme_info to <your theme>.info, IE: mysubtheme.info.
3) Edit <your theme>.info and change the name of the theme.
