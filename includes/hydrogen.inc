<?php

class hydrogen {
   
  function defaults($settings, $global_settings = array()) {
    $defaults = array(
      'user_notverified_display'              => 1,
      'breadcrumb_display'                    => 0,
      'rebuild_registry'                      => 0,
      'use_nice_menus'                        => 0,
      'admin_remove_sidebars'                 => 0,
      'grid_layout'                           => 'sidebar_second',
      'sidebar_first_width'                   => 3,
      'sidebar_second_width'                  => 3,
      'inset_column_width'                    => 3,
      'wider_column'                          => 0,
      'container'                             => 16,
      'submitted_use_default'                 => 1,
      'default_submitted_string'              => 'Submitted by !author on !date', 
      'default_submitted_date_format'         => 'default',
      'use_default_layout'                    => 1,
    );

    $defaults = array_merge($defaults, $settings); 

    // Set the default values for content-type-specific settings
    foreach (array('default' => 'Default') + node_get_types('names') as $type => $name) {
      if ($type <> 'default') {
        $defaults["toggle_node_info_{$type}"] = $global_settings["toggle_node_info_{$type}"];
      }
      $defaults["{$type}_submitted_use_default"] = $defaults['submitted_use_default'];
      $defaults["{$type}_submitted_date_format"] = $defaults['default_submitted_date_format'];
      $defaults["{$type}_submitted_string"] = $defaults['default_submitted_string'];
      $defaults["{$type}_container"] = $defaults['container'];
      $defaults["{$type}_grid_layout"] = $defaults['grid_layout'];
      $defaults["{$type}_sidebar_first_width"] = $defaults['sidebar_first_width'];
      $defaults["{$type}_sidebar_second_width"] = $defaults['sidebar_second_width'];
      $defaults["{$type}_inset_column_width"] = $defaults['inset_column_width'];
      $defaults["{$type}_wider_column"] = $defaults['wider_column'];
      $defaults["{$type}_use_default_layout"] = $defaults['use_default_layout'];
    }
    $settings = array_merge($defaults, $settings);
    
    return $settings; 
  }

  /**
   * Method to present a submitted by settings form.
   * 
   * @param $settings
   *   Current settings for each form element.
   * @param $type
   *   Optional type or context to configure a layout for.
   * @param $status
   *   Optional flag for the current state of form elements.
   * @return $form
   *   Returns a settings form for configuring submitted by options.
   */
  function submitted_settings(&$form, $settings, $type = 'default', $status = FALSE) {
    $date_formats = array();
    $date_formats['default'] = t('Default');
    if (module_exists('date')) {
      $formats = date_get_format_types('', TRUE);
      if (!empty($formats)) {
        foreach ($formats as $format => $type_info) {
          $date_formats[$format] = $type_info['title'];
        }
      }
    } 
    else {
      $date_formats += array(
        'small' => t('Small'),
        'medium' => t('Medium'),
        'large' => t('Large'),
        'custom' => t('Custom'),
      );
    }

    if ($type <> 'default') {
      $form[$type]["toggle_node_info_{$type}"] = array(
        '#title' => t("Show post information for this content type."),
        '#type' => 'checkbox',
        '#default_value' => $settings["toggle_node_info_{$type}"],
      );
      if ($settings["{$type}_submitted_use_default"]) $status = TRUE;
      $form[$type]["{$type}_submitted_use_default"] = array(
        '#type' => 'checkbox',
        '#title' => t('Use default settings for post information.'),
        '#default_value' => $settings["{$type}_submitted_use_default"], 
      );
    }
    
    $form[$type]['submitted_by'] = array(
      '#title' => t('Post information'),
      '#type' => 'fieldset',
      '#disabled' => $status,
      '#collapsible' => TRUE,
      '#collapsed' => $status,
    );

    $form[$type]['submitted_by']["{$type}_submitted_date_format"] = array(
      '#type'          => 'select',                                                                                   
      '#title'         => t('Date format'),                                                                      
      '#options'       => $date_formats, 
      '#default_value' => ($status || !$settings["{$type}_submitted_date_format"]) ? $settings['default_submitted_date_format'] : $settings["{$type}_submitted_date_format"],
      '#disabled' => $status,
    ); 

    // @todo For some reason the default value is not getting set properly if this is overridden.
    $form[$type]['submitted_by']["{$type}_submitted_string"] = array(
      '#type'          => 'textfield',                                                                                    
      '#title'         => t('Format string for submitted by.'),
      '#description'   => t('A string used to format the post information on a node. Use !author to substitute in the author of the post, and !date along with a selected date format to present date information on when the post was created.'),
      '#default_value' => ($status || !$settings["{$type}_submitted_string"]) ? $settings['default_submitted_string'] : $settings["{$type}_submitted_string"],
      '#disabled' => $status,
    );                                              
  } 

  /**
   * Method to present a layout settings form.
   * 
   * @param $settings
   *   Current settings for each form element.
   * @param $type
   *   Optional type or context to configure a layout for.
   * @param $status
   *   Optional flag for the current state of form elements.
   * @return $form
   *   Returns a settings form for configuring layout options.
   */
  function layout_settings(&$form, $settings, $type = 'default', $status = FALSE) {
    // Layout settings
    $layout_form['layout'] = array(
      '#type' => 'fieldset',
      '#title' => t('Layout'),
      '#collapsible' => TRUE,
      '#collapsed' => $status,
      '#attributes' => array('cs' => 'layout_settings'),
    );
    $layout_form['layout']["{$type}_container"] = array(
      '#type' => 'select',
      '#title' => t('Select the number of columns in the grid.'),
      '#options' => array(
        '16' => '16 columns',
        '12' => '12 columns',
      ),
      '#default_value' => ($status || !$settings["{$type}_container"]) ? $settings['default_container'] : $settings["{$type}_container"],
      '#disabled' => $status,
    );
    $layout_form['layout']["{$type}_grid_layout"] = array(
      '#type' => 'radios',
      '#title' => t('Select the grid layout'),
      '#options' => array(
        'sidebar_second' => theme_image(drupal_get_path('theme', 'hydrogen') . '/includes/images/sidebar_second.png'),
        'sidebar_first'  => theme_image(drupal_get_path('theme', 'hydrogen') . '/includes/images/sidebar_first.png'), 
        'equal' => theme_image(drupal_get_path('theme', 'hydrogen') . '/includes/images/equal.png'), 
      ),
      '#default_value' => ($status || !$settings["{$type}_grid_layout"]) ? $settings['default_grid_layout'] : $settings["{$type}_grid_layout"],
      '#suffix' => '<div cs="clear"></div>',
      '#attributes' => array('cs' => 'grid-layout'),
      '#disabled' => $status,
    );
    $layout_form['layout']["{$type}_sidebar_first_width"] = array(
      '#type' => 'textfield',
      '#title' => t('First column width'),
      '#default_value' => ($status || !$settings["{$type}_sidebar_first_width"]) ? $settings["default_sidebar_first_width"] : $settings["{$type}_sidebar_first_width"],
      '#disabled' => $status,
    );
    $layout_form['layout']["{$type}_sidebar_second_width"] = array(
      '#type' => 'textfield',
      '#title' => t('Last column width'),
      '#default_value' => ($status || !$settings["{$type}_sidebar_second_width"]) ? $settings['default_sidebar_second_width'] : $settings["{$type}_sidebar_second_width"],
      '#disabled' => $status,
    );
    $layout_form['layout']["{$type}_inset_column_width"] = array(
      '#type' => 'textfield',
      '#title' => t('Inset column width'),
      '#default_value' => ($status || !$settings["{$type}_inset_column_width"]) ? $settings['default_inset_column_width'] : $settings["{$type}_inset_column_width"],
      '#disabled' => $status,
    );
    $layout_form['layout']["{$type}_wider_column"] = array(
      '#type' => 'checkbox',
      '#title' => t('Make single sidebar one column wider.'),
      '#description' => t('On pages where only one side bar is shown, increase the width of the side bar by 1 column.'), 
      '#default_value' => ($status || !$settings["{$type}_wider_column"]) ? $settings['default_wider_column'] : $settings["{$type}_wider_column"],
      '#disabled' => $status,
    );
    if (isset($type)) {
      $form[$type] = array_merge($form[$type], array($type => $layout_form));
    }
    else {
      $form = array_merge($form, $layout_form);
    }
  }
}
