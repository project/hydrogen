<?php

/**
* Implementation of THEMEHOOK_settings() function.
*
* @param $saved_settings
*   array An array of saved settings for this theme.
* @return
*   array A form array.
*/
function phptemplate_settings($saved_settings) {
  // Include hydrogen 
  include_once(drupal_get_path('theme', 'hydrogen') .'/includes/hydrogen.inc');  
  $settings = hydrogen::defaults(theme_get_settings(), variable_get('theme_settings', array()));

  // Strip toggle_node_info_% from the saved theme settings. 
  if (module_exists('node')) {
    foreach (node_get_types() as $type => $name) {
      unset($saved_settings['toggle_node_info_'. $type]);
    }
  }
  
  $settings = array_merge($settings, $saved_settings);

  // Create theme settings form widgets using Forms API
  // General Settings
  $form['general_configuration'] = array(
    '#type' => 'fieldset',
    '#title' => t('General configuration'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#attributes' => array('cs' => 'general_configuration'),
  );
  
  // Breadcrumb
  $form['general_configuration']['breadcrumb_display'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display breadcrumb'),
    '#default_value' => $settings['breadcrumb_display'],
  );

  // Theme menu links as nice menus
  if (module_exists('nice_menus')) {
    $form['general_configuration']['use_nice_menus'] = array(
      '#type' => 'checkbox',
      '#title' => t('Display primary links using Nice Menus'),
      '#default_value' => $settings['use_nice_menus'],
    );
  }
  
  // Remove sidebars in admin area
  $form['general_configuration']['admin_remove_sidebars'] = array(
    '#type' => 'select',
    '#options' => array(
      '0' => t('No'),
      'sidebar_first' => t('Left'),
      'sidebar_second' => t('Right'),
      'both' => t('Both'),
    ),
    '#title' => t('Remove sidebars in admin areas'),
    '#default_value' => $settings['admin_remove_sidebars'],
  );
  // Development settings
  $form['general_configuration']['themedev'] = array(
    '#type' => 'fieldset',
    '#title' => t('Theme development'),
    '#collapsible' => TRUE,
    '#collapsed' => $settings['rebuild_registry'] ? FALSE : TRUE,
  );
  $form['general_configuration']['themedev']['rebuild_registry'] = array(
    '#type' => 'checkbox',
    '#title' => t('Rebuild theme registry on every page.'),
    '#default_value' => $settings['rebuild_registry'],
    '#description' => t('During theme development, it can be very useful to continuously <a href="!link">rebuild the theme registry</a>. WARNING: this is a huge performance penalty and must be turned off on production websites.', array('!link' => 'http://drupal.org/node/173880#theme-registry')),
  );

  // Default & content-type specific settings
  foreach ((array('default' => 'Default') + node_get_types('names')) as $type => $name) {
    $form[$type] = array(
      '#type' => 'fieldset',
      '#title' => t('!name settings', array('!name' => t($name))),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    
    // Load the form for modifiying submitted by settings.
    hydrogen::submitted_settings($form, $settings, $type);

    if ($type <> 'default') {
      $form[$type]["{$type}_use_default_layout"] = array(                                                              
        '#type' => 'checkbox',                                                                                             
        '#title' => t('Use default layout for !name.', array('!name' => t($name))),
        '#default_value' => $settings["{$type}_use_default_layout"],
      );
    }

    if (($type == 'default') || (!$settings["{$type}_use_default_layout"])) {
      $status = FALSE;
    }
    else {
      $status = TRUE;
    }
    hydrogen::layout_settings($form, $settings, $type, $status); 
  }

  // Vertical tab-ify this form if vertical_tabs module is installed.
  $form['#pre_render'][] = 'vertical_tabs_form_pre_render';

  // Return theme settings form
  return $form;
}  
