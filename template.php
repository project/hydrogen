<?php

/**
 * Initialize theme settings
 */
if (is_null(theme_get_setting('user_notverified_display')) || theme_get_setting('rebuild_registry')) {
  include_once(drupal_get_path('theme', 'hydrogen') .'/includes/hydrogen.inc');
  global $theme_key;
	
  // Auto-rebuild the theme registry during theme development.
  if(theme_get_setting('rebuild_registry')) {
    drupal_set_message(t('The theme registry has been rebuilt. <a href="!link">Turn off</a> this feature on production websites.', array('!link' => url('admin/build/themes/settings/' . $GLOBALS['theme']))), 'warning');
  }
  
  // Get default theme settings.
  $settings = theme_get_settings($theme_key);
  $global_settings = variable_get('theme_settings', array());
  $merged_settings = hydrogen::defaults($settings, $global_settings); 
  
  // Don't save the toggle_node_info_ variables
  if (module_exists('node')) {
    foreach (node_get_types() as $type => $name) {
      $new_global_settings['toggle_node_info_'. $type] = $settings['toggle_node_info_'. $type];
      unset($settings['toggle_node_info_'. $type]);
    }
  }
  
  // Update global theme settings
  variable_set('theme_settings', array_merge($global_settings, $new_global_settings));

  // Save default theme settings
  variable_set(
    str_replace('/', '_', 'theme_'. $theme_key .'_settings'),
    $merged_settings 
  );

  // Force refresh of Drupal internals
  theme_get_setting('', TRUE);
}

/**
 * Modify theme variables
 */
function hydrogen_preprocess(&$vars) {
  global $user;                                            // Get the current user
  $vars['is_admin'] = in_array('admin', $user->roles);     // Check for Admin, logged in
  $vars['logged_in'] = ($user->uid > 0) ? TRUE : FALSE;
}

/**
 * Implementation of template_preprocess_page().
 */
function hydrogen_preprocess_page(&$vars, $hook) {
  // For easy printing of variables.
  $vars['logo_img'] = $vars['logo'] ? theme('image', substr($vars['logo'], strlen(base_path())), t('Home'), t('Home')) : '';
  $vars['linked_logo_img'] = $vars['logo_img'] ? l($vars['logo_img'], '<front>', array('attributes' => array('rel' => 'home'), 'title' => t('Home'), 'html' => TRUE)) : '';
  $vars['linked_site_name'] = $vars['site_name'] ? l($vars['site_name'], '<front>', array('attributes' => array('rel' => 'home'), 'title' => t('Home'))) : '';
  $vars['main_menu_links'] = theme('links', $vars['primary_links'], array('class' => 'links main-menu'));
  $vars['secondary_menu_links'] = theme('links', $vars['secondary_links'], array('class' => 'links secondary-menu'));

  // Make sure grid framework style sheets are placed above all others.
  $vars['css_alt'] = hydrogen_css_reorder($vars['css']);
  $vars['styles'] = drupal_get_css($vars['css_alt']);

  // Hide breadcrumbs on all pages
  if (theme_get_setting('breadcrumb_display') == 0) {
    $vars['breadcrumb'] = '';
  }

  // Re-theme primary links using Nice Menus
  if (theme_get_setting('use_nice_menus') && module_exists('nice_menus')) {
    $vars['main_menu_links'] = theme_nice_menus_primary_links();
    $vars['secondary_menu_links'] = '';
  }

  // Set up layout variable.
  $variables['layout'] = 'none';
  if (!empty($variables['sidebar_first'])) {
    $variables['layout'] = 'sidebar-first';
  }
  if (!empty($variables['sidebar_second'])) {
    $variables['layout'] = ($variables['layout'] == 'sidebar-first') ? 'both' : 'sidebar-second';
  }

  // Remove sidebars in Admin area
  if (($value = theme_get_setting('admin_remove_sidebars')) && (substr($_GET['q'], 0, 5) == 'admin') && (substr($_GET['q'], 12, 5) <> 'block')) {
    if (in_array($value, array('sidebar_first','sidebar_second'))) {
      $vars[$value] = '';
    }
    elseif ($value == 'both') {
      $vars['sidebar_first'] = $vars['sidebar_second'] = '';
    }
  }

  // Add a subtitle if one exists
  if (!empty($vars['node']) && $node = $vars['node']) {
    $vars['subtitle'] = $node->field_subtitle[0]['value'];
  }
  else {
    $vars['subtitle'] = '';
  }

  // Assign the current node type as a context for driving layout.
  // @todo In the future we may want to add other contexts for driving layout.
  if (isset($vars['node'])) {
    $context = $vars['node']->type;
  }

  // Configure number of columns from settings.
  $vars['container'] = hydrogen_layout_setting('container', $context);

  // Configure grid layout from settings.
  $vars['grid_layout'] = $grid_layout = hydrogen_layout_setting('grid_layout', $context);
 
  // Get configurable region widths from theme settings.
  $columns['sidebar_first']  = hydrogen_layout_setting('sidebar_first_width', $context);
  $columns['sidebar_second'] = hydrogen_layout_setting('sidebar_second_width', $context);
  $columns['inset'] = hydrogen_layout_setting('inset_column_width', $context);

  // Set our grid container.
  $container = hydrogen_layout_setting('container', $context);

  // Get other theme settings which affect layout.
  $wide_single = (bool) theme_get_setting('wider_column');

  if ($wide_single && !$vars['sidebar_first']) $columns['sidebar_second'] = $columns['sidebar_second'] + 1;
  if ($wide_single && !$vars['sidebar_second']) $columns['sidebar_first'] = $columns['sidebar_first'] + 1;

  // Build layout and set up variables for grid sizing.
  hydrogen_layout($vars, $grid_layout, $columns, $container);  

  // Get current body classes and remove the no-sidebars class added by core
  $body_classes = explode(' ', str_replace('no-sidebars', '', $vars['body_classes']));

  // Remove any empty values from the explode
  $body_classes = array_filter($body_classes);
  
  // Add body classes for our sidebars.
  if ($vars['sidebar_first'] && !$vars['sidebar_second']) {
    $body_classes[] = 'sidebar-first';
    $body_classes[] = 'one-sidebar'; 
  }
  elseif (!$vars['sidebar_first'] && $vars['sidebar_second'])  { 
    $body_classes[] = 'sidebar-second';
    $body_classes[] = 'one-sidebar'; 
  }
  elseif ($vars['sidebar_first'] && $vars['sidebar_second']) {
    $body_classes[] = 'two-sidebars'; 
  }
  else {
    $body_classes[] = 'no-sidebars';
  }

  // Add a class for grid layout.
  $body_classes[] = 'layout-'. form_clean_id($vars['grid_layout']);

  $vars['body_classes'] = implode(' ', $body_classes);
}

/**
 * Wrapper for theme_get_setting().
 */
function hydrogen_layout_setting($setting, $context = '') {
  if ($context) {
    if (theme_get_setting($context .'_enable_layout_override')) {
      return theme_get_setting($context .'_'. $setting);
    }
  }
  return theme_get_setting('default_'. $setting);
}

/**
 * Internal callback for handling grid layout
 */
function hydrogen_layout(&$vars, $grid_layout, $columns = array(), $container = '') {
  // Make sure the variable we're working with is the one that was passed to the function
  $vars['grid_layout'] = $grid_layout;

  // The number of columns to work with.
  $vars['container'] = $c = $container ? $container : hydrogen_layout_setting('container'); 

  // content wrapper 
  if ($grid_layout == 'sidebar_second' || $grid_layout == 'both_sidebar_second') {
    $vars['grid_content_wrapper'] = hydrogen('grid-'. $c, $vars['sidebar_second'], $columns['sidebar_second']);
  }
  elseif($grid_layout == 'sidebar_first') {
    // $vars['grid_content_wrapper'] = hydrogen('grid-16', $vars['sidebar_first'], $columns['sidebar_first']);
    $vars['grid_content_wrapper'] = '';
  }
  else {
    $vars['grid_content_wrapper'] = 'container-'. $c;
  }

  // sub feature
  if (in_array($grid_layout, array('sidebar_second', 'both_sidebar_second'))) {
    $vars['grid_sub_feature'] = hydrogen('grid-'. $c, $vars['sidebar_second'], $columns['sidebar_second']) .' alpha omega';
  }
  elseif ($grid_layout == 'sidebar_first')  {
    $vars['grid_sub_feature'] = hydrogen('grid-'. $c, $vars['sidebar_first'], $columns['sidebar_first']) .' omega';
  }
  else {
    $vars['grid_sub_feature'] = 'container-'. $c .' alpha omega';
  }

  // sidebar_first side bar
  $vars['grid_sidebar_first'] = 'grid-'. $columns['sidebar_first']; // .' alpha';
  if ($grid_layout == 'sidebar_second') {
    $vars['grid_sidebar_first'] = $vars['grid_sidebar_first'] . ' alpha';
  }

  // main
  $vars['grid_main'] = hydrogen('grid-'. $c, $vars['sidebar_first'], $columns['sidebar_first'], $vars['sidebar_second'], $columns['sidebar_second']); // .' omega';
  if ($grid_layout == 'sidebar_second' || $grid_layout == 'both_sidebar_second') {
    $vars['grid_main'] = $vars['grid_main'] . ' omega';
    if (!$vars['sidebar_first']) {
      $vars['grid_main'] = $vars['grid_main'] .' alpha';
    }
  }

  // inset column
  $vars['grid_inset'] = 'grid-'. $columns['inset'] .' omega';

  // sidebar_second side bar 
  $vars['grid_sidebar_second'] = hydrogen('grid-'. $columns['sidebar_second']);
  
  // Additional definitions.
  $vars['grid_page'] = 'container-'. $c;
  $vars['grid_header_region'] = 'grid-'. $c;
  $vars['grid_branding'] = 'grid-' . $c;
  $vars['grid_logo'] = '';
  $vars['grid_site_name'] = '';
  $vars['grid_site_slogan'] = '';
  $vars['grid_mission'] = '';
  $vars['grid_site_menu'] = 'grid-'. $c;
  $vars['grid_feature'] = 'container-'. $c;
  $vars['grid_footer'] = 'container-'. $c;
  $vars['grid_footer_message'] = 'grid-'. $c;
}

/**
 * Contextually adds 960 Grid System classes. 
 * Derived from the Ninesixty theme ns() function.
 *
 * The first parameter passed is the *default class*. All other parameters must
 * be set in pairs like so: "$variable, 3". The variable can be anything available
 * within a template file and the integer is the width set for the adjacent box
 * containing that variable.
 *
 *  class="<?php print hydrogen('grid-16', $var_a, 6); ?>"
 *
 * If $var_a contains data, the next parameter (integer) will be subtracted from
 * the default class. See the README.txt file.
 */
function hydrogen() {
  $args = func_get_args();
  $default = array_shift($args);
  // Get the type of class, i.e., 'grid', 'pull', 'push', etc.
  // Also get the default unit for the type to be procesed and returned.
  list($type, $return_unit) = explode('-', $default);

  // Process the conditions.
  $flip_states = array('var' => 'int', 'int' => 'var');
  $state = 'var';
  foreach ($args as $arg) {
    if ($state == 'var') {
      $var_state = !empty($arg);
    }
    elseif ($var_state) {
      $return_unit = $return_unit - $arg;
    }
    $state = $flip_states[$state];
  }

  $output = '';
  // Anything below a value of 1 is not needed.
  if ($return_unit > 0) {
    $output = $type . '-' . $return_unit;
  }
  return $output;
}

/**
 * Adds a class for the style of view  
 * (e.g., node, teaser, list, table, etc.)
 * (Requires views-view.tpl.php file in theme directory)
 */
function hydrogen_preprocess_views_view(&$vars) {
  $vars['css_name'] = $vars['css_name'] .' view-style-'. views_css_safe(strtolower($vars['view']->type));
}

/**
 * This rearranges how the style sheets are included so the framework styles
 * are included first.
 *
 * Sub-themes can override the framework styles when it contains css files with
 * the same name as a framework style. This can be removed once Drupal supports
 * weighted styles.
 */
function hydrogen_css_reorder($css) {
  global $theme_info, $base_theme_info;

  // Dig into the framework .info data.
  $framework = !empty($base_theme_info) ? $base_theme_info[0]->info : $theme_info->info;

  // Pull framework styles from the themes .info file and place them above all stylesheets.
  if (isset($framework['stylesheets'])) {
    foreach ($framework['stylesheets'] as $media => $styles_from_960) {
      // Setup framework group.
      if (isset($css[$media])) {
        $css[$media] = array_merge(array('framework' => array()), $css[$media]);
      }
      else {
        $css[$media]['framework'] = array();
      }
      foreach ($styles_from_960 as $style_from_960) {
        // Force framework styles to come first.
        if (strpos($style_from_960, 'framework') !== FALSE) {
          $framework_shift = $style_from_960;
          $remove_styles = array($style_from_960);
          // Handle styles that may be overridden from sub-themes.
          foreach ($css[$media]['theme'] as $style_from_var => $preprocess) {
            if ($style_from_960 != $style_from_var && basename($style_from_960) == basename($style_from_var)) {
              $framework_shift = $style_from_var;
              $remove_styles[] = $style_from_var;
              break;
            }
          }
          $css[$media]['framework'][$framework_shift] = TRUE;
          foreach ($remove_styles as $remove_style) {
            unset($css[$media]['theme'][$remove_style]);
          }
        }
      }
    }
  }

  return $css;
}

/**
 * Implementation of theme_node_submitted().
 */
function hydrogen_node_submitted($node) {
  $type = $node->type;
  if (!theme_get_setting($type .'_submitted_use_default')) {
    $string = theme_get_setting($type .'_submitted_string', theme_get_setting('default_submitted_string'));  
    $format = theme_get_setting($type .'_submitted_date_format', theme_get_setting('default_submitted_date_format'));
  }
  else {
    $string = theme_get_setting('default_submitted_string');
    $format = theme_get_setting('default_submitted_date_format');
  }
  $default = variable_get('date_format_medium',  'D, m/d/Y - H:i');
  $format = variable_get('date_format_'. $format, $default);

  if (module_exists('date')) {
    $date = date_format_date(date_make_date($node->created, NULL, DATE_UNIX), 'custom', $format);
  }
  else {
    $date = format_date($node->created, 'custom', $format);
  }

  $replace = array(
    '!author' => l($node->name, 'user/'. $node->uid),
    '!date'   => $date, 
  );
  return filter_xss(t($string, $replace)); 
}

